global clipboardDataOld, clipboardDataNew -- put here all the necessary variables for the loop.

on run
	do shell script "pbcopy < /dev/null" # clear clipboard on start
	set clipboardDataOld to ""
	set clipboardDataNew to ""
end run

on idle
	tell application "System Events"
		try
			set clipboardDataOld to clipboardDataNew
			set clipboardDataNew to (the clipboard as text)
			# clear clipboard only if there no one copied anything since previous iteration
			if clipboardDataOld = clipboardDataNew and clipboardDataNew is not equal to "" then
				do shell script "pbcopy < /dev/null" # clear clipboard
			end if
		on error errorMessage number the errorNumber
			set res to (errorMessage & errorNumber as text)
			do shell script "logger -p local3.error " & res # write to a system log
		end try
	end tell
	return 300 # wait for 5 minutes
end idle


on quit
	continue quit
end quit
