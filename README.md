# clipxfire

- background application that makes sure your clipboard data expires after specified period of time
- tested on macOS Big Sur 11.5.2


## compiling app from sources

- clone repository or save clipxfire.scpt file onto your local drive
- open clipxfire.scpt with Script Editor app (double clicking on file will do)
- compile and save script as clipxfire.app in universal application format
    - select "File -> Export..." and specify following values in the dialog
    - "Export As" set to "clipxfire"
    - "File Format" set to "Application"
    - "Options"
        - select "Stay open after run handler"
        - select "Run-only"
    - "Code Sign" set to "Sign to Run Locally"
    - click "Save" button
- make app run at user login (optional)
    - select "System Preferences" -> "Users & Groups" -> "Current User" -> "Login Items" tab
    - click "+" button to add new login item
    - select previously saved clipxfire.app file
        - select "hide" checkbox for newly added login item
- run clipxfire.app
    - there may be a dialog with the following message:
        “clipxfire” wants access to control “System Events”. 
        Allowing control will provide access to documents and data in “System Events”, 
        and to perform actions within that app. This script needs to control other applications to run.
    - click "OK"


## adjusting expiration time

you can update "return 300" part of script to whatever you like, default value means 300 seconds or 5 minutes


## references

- https://discussions.apple.com/thread/6615657 (see post by Jacques Rioux)
- https://superuser.com/questions/99716/run-applescript-on-startup-background
- https://apple.stackexchange.com/questions/55146/is-there-an-easy-way-to-clear-empty-the-clipboard
- http://www.quickmeme.com/p/3r0fn2
- https://imgflip.com/memegenerator/Paranoid-Parrot
